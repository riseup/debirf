# Riseup debirf Makefile

MKDEBIRF = ../bin/mkdebirf

foo:
	(cd profiles;\
	$(MKDEBIRF) )

foo-i386:
	(cd profiles;\
	$(MKDEBIRF) -a i386 -k 686 )

# these aren't working yet
amd64:
	(cd profiles;\
	$(MKDEBIRF); \
	$(MKDEBIRF) -n eth1 -r skip; \
	\
	$(MKDEBIRF) -s wheezy; \
	$(MKDEBIRF) -s wheezy -n eth1 -r skip; \
	$(MKDEBIRF) -s sid; \
	$(MKDEBIRF) -s sid -n eth1 -r skip )

i386:
	(cd profiles;\
	$(MKDEBIRF) -a i386 -k 686; \
	$(MKDEBIRF) -a i386 -k 686 -n eth1 -r skip; \
	$(MKDEBIRF) -s wheezy -a i386 -k 686; \
	$(MKDEBIRF) -s wheezy -a i386 -k 686 -n eth1 -r skip; \
	$(MKDEBIRF) -s sid -a i386 -k 686; \
	$(MKDEBIRF) -s sid -a i386 -k 686 -n eth1 -r skip )

# need to write these still
clean-amd64:
clean-i386:

clean: clean-amd64 clean-i386
	rm -f profiles/*/.bootstrap.log
	rm -f profiles/*/.fakeroot-state.debirf
	rm -f profiles/*/debirf_*.cgz
	rm -f profiles/*/vmlinuz-*

reallyclean: clean
	rm -rf profiles/*/nest/
	rm -rf profiles/*/root/

.PHONY: amd64 i386 clean reallyclean
